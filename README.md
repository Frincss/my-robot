Bom depois de vários testes e pelo menos 3 robôs anteriores, pra me acostumar com os métodos, estratégias e tals, eu lhes apresento minha maquina de destruição e caos com o nome de um animalzinho fofo, completamente não clichê.

```
package meusrobos;
import robocode.*;
import java.awt.Color;


public class Julie extends AdvancedRobot
{
	public void run() {
		setColors(Color.BLACK, Color.BLACK, Color.GREEN, Color.GREEN, Color.BLACK);
		
		while(true) {
			setAhead(1000);
			setTurnRight(300);
			setTurnGunLeft(1000); 
			setTurnLeft(300);
			setTurnGunRight(1000);
			execute();
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		if(e.getDistance() < 40 && getEnergy() >= 60)
			fire(3);
		else
			fire(2);

		scan();
	}

	public void onHitByBullet(HitByBulletEvent e) {
		setTurnRight(1000);
	}

	public void onHitWall(HitWallEvent e) {
		setTurnRight(1000);
	}
}
```

Creio eu que essa parte não precise de um comentário aprofundado já que o package e a pasta obrigatória onde seu projeto sera guardado e os outros são imports padrões e sem isso nenhum projeto teria acesso as classes necessárias para a criação e customização de seu robô respectivamente.

```
package hibrido;
import robocode.*;
import java.awt.Color;
```


Agora as coisas começam a ficar interessantes, por padrão um robô estende a classe Robot para ter acesso aos movimentos bases, porem entretanto toda via eu preferi pela classe AdvanceRobot já que ela conseguia um gingado bem diferente da anterior, eu explico melhor isso no próximo tópico.

```
public class Julie extends AdvancedRobot
{
```


Roda roda e vira solta a roda e ve...Continuando, esses três métodos compõe toda a movimentação e customização xbox da Julie, o seu gingado faz com que ela foque girando o corpo no mesmo eixo ate colidir com algum robô, bala ou parede aumentando a velocidade do giro e fazendo essa menina rodopiar pela arena igual a uma beyblade, já seu canhão gira no sentido contrario conseguindo cobrir o campo inteiro e atirar pra todos os lados.

```
public void run() {
	setColors(Color.BLACK, Color.BLACK, Color.GREEN, Color.GREEN, Color.BLACK);
		
	while(true) {
		setAhead(1000);
		setTurnRight(300);
		setTurnGunLeft(1000); 
		setTurnLeft(300);
		setTurnGunRight(1000); 
		execute();
	}
}
public void onHitByBullet(HitByBulletEvent e) {
	setTurnRight(1000);
}

public void onHitWall(HitWallEvent e) {
	setTurnRight(1000);
}
```


Essa parte do código foi a primeira implementação que eu pensei e todas as minhas criações tem, e um método simples para controlar a quantidade de energia gasta pelos tiros sem diminuir o poder de fogo, ela faz isso usando uma verificação simples pra aumentar as chances de acertar o tiro mais potente e não gastar energia atoa, depois disso o scanner e chamado mais uma vez pra tentar um novo tiro, a parte negativa e que se a batalha se estender demais ela vai perder a energia e vai se tornar um alvo fácil e que não revida, basicamente um tijolo com rodas. 

```
public void onScannedRobot(ScannedRobotEvent e) {
	if(e.getDistance() < 40 && getEnergy() >= 60)
		fire(3);
	else
		fire(2);

	scan();
} 
```


A maior força dessa menina esta na sua esquiva e no seu canhão, a estratégia de rodopios pela arena garantem uma movimentação rápida oque e perfeito para desviar de balas e se manter longe de seus oponentes, junte isso com tiros de nível 2 e 3 e um radar que percorre toda o lugar e temos um robô perfeito contra uma estratégia de aproximação, mas nem tudo são flores, nos testes que fiz contra o Walls ficou claro que ela sofre bastante contra oponentes que se afastam rápido demais ou com uma estratégia para desviar de balas, já que como os tiros dados são de nível 2 pra cima a perda de uma grande quantidade de energia e inevitável.

Uma curiosidade pra fechar o texto, Essa sequencia de chamada de metodos tem uma historia curiosa, eu estava testando os movimentos da Julie e tentei fazer ela andar na forma de um 8, no meio do caminho eu desisti da ideia porque fazia ela travar nas quinas, mas ai eu esqueci de apagar essa sequencia e quando fui fazer o teste final, 1000 rounds contra o robô mais forte dos samples o Walls, ela se saiu 20% melhor que nas outras vezes e quando eu percebi foi por causa disso, eu não vou saber explicar direito porque mas funcionou melhor que antes.

```
setAhead(1000);
setTurnRight(300);
setTurnGunLeft(1000); 
setTurnLeft(300);
setTurnGunRight(1000); 
execute();
```

