package hibrido;
import robocode.*;
import java.awt.Color;

public class Julie extends AdvancedRobot
{
	public void run() {
		setColors(Color.BLACK, Color.BLACK, Color.GREEN, Color.GREEN, Color.BLACK);
		
		while(true) {
			setAhead(1000);
			setTurnRight(300);
			setTurnGunLeft(1000);
			setTurnLeft(300);
			setTurnGunRight(1000);
			execute();
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		if(e.getDistance() < 40 && getEnergy() >= 60)
			fire(3);
		else
			fire(2);	
		
		scan();
	}

	public void onHitByBullet(HitByBulletEvent e) {
		setTurnRight(1000);
	}

	public void onHitWall(HitWallEvent e) {		
		setTurnRight(1000);
	}	
}
